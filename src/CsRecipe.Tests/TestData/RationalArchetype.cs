using Rationals;

namespace CsRecipe.Tests.TestData;

internal static class RationalArchetype
{
	public static Rational SomeValue => 2;
	public static Rational SomeOtherValue => -1;
	public static Rational YetAnotherValue => 5;
}
