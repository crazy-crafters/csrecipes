using System.Numerics;
using FsCheck;
using FsCheck.Fluent;
using Rationals;

public class Arbitraries
{
	public static Arbitrary<Rational> Rational()
	{
		return Arb.From(from p in ArbMap.Default.GeneratorFor<BigInteger>()
			from q in ArbMap.Default.GeneratorFor<BigInteger>()
			where q != 0
			select new Rational(p, q));
	}
}
