using CsRecipe.Quantities;
using Rationals;

namespace CsRecipe.Tests.TestData;

internal static class ConverterArchetype
{
    public static Converter SomeValue(Rational someValue) => Converter.Affine(someValue, Rational.Zero);
    public static Converter SomeOtherValue(Rational someValue) => Converter.Affine(Rational.One, someValue);
}
