using Rationals;

namespace CsRecipe.Tests;

internal class RationalsApparently
{
	[Test]
	public void Creates_Rational_From_String_Type()
	{
		var half = new Rational(1, 2);
		Assert.That(
			Rational.Parse("1/2"),
			Is.EqualTo(half));
	}

	public void Creates_Rational_From_Either_Numerator_And_Denominator_Either_Numerator_With_Division()
	{
		Assert.That(
			new Rational(1, 2),
			Is.EqualTo(new Rational(1) / 2));
	}
}
