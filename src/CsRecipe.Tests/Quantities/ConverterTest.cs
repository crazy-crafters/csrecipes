using CsRecipe.Quantities;
using CsRecipe.Tests.TestData;
using FsCheck;
using FsCheck.Fluent;
using Rationals;
using static CsRecipe.Quantities.Converter;

namespace CsRecipe.Tests.Quantities;

public class ConverterTest
{
    [Test]
    public void Identity_should_be_identity_on_to_operation()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            (value) => Identity().To(value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Identity_should_be_identity_on_from_operation()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            (value) => Identity().From(value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Identity_should_be_stable_on_roundtrip()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            (value) => ConvertRoundtrip(Identity(), value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Scale_should_multiply_on_to_operation()
    {
        Assert.That(Scale(2).To(4), Is.EqualTo(8));
    }

    [Test]
    public void Scale_should_divide_on_from_operation()
    {
        Assert.That(Scale(2).From(8), Is.EqualTo(4));
    }

    [Test]
    public void Scale_should_be_stable_on_roundtrip()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            Arbitraries.Rational().Filter(v => v != Rational.Zero),
            (value, factor) => ConvertRoundtrip(Scale(factor), value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Offset_should_add_on_to_operation()
    {
        Assert.That(Offset(2).To(4), Is.EqualTo(6));
    }

    [Test]
    public void Offset_should_substract_on_from_operation()
    {
        Assert.That(Offset(2).From(6), Is.EqualTo(4));
    }

    [Test]
    public void Offset_should_be_stable_on_roundtrip()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            Arbitraries.Rational(),
            (value, delta) => ConvertRoundtrip(Offset(delta), value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Inverse_should_inverse_to_operation_into_from_operation()
    {
        Assert.That(Inverse(Offset(2)).To(4), Is.EqualTo(2));
    }

    [Test]
    public void Inverse_should_inverse_from_operation_into_to_operation()
    {
        Assert.That(Inverse(Offset(2)).From(2), Is.EqualTo(4));
    }

    [Test]
    public void Inverse_should_be_stable_on_roundtrip()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            Arbitraries.Rational(),
            (value, delta) => ConvertRoundtrip(Inverse(Offset(delta)), value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Compose_should_apply_the_first_converter_and_the_second_converter_on_to_operation()
    {
        Assert.That(Compose(Scale(3), Offset(2)).To(5), Is.EqualTo(17));
    }

    [Test]
    public void Compose_should_apply_the_second_converter_and_the_first_converter_on_from_operation()
    {
        Assert.That(Compose(Scale(3), Offset(2)).From(17), Is.EqualTo(5));
    }

    [Test]
    public void Compose_should_be_stable_on_roundtrip()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            Arbitraries.Rational().Filter(v => v != Rational.Zero),
            Arbitraries.Rational(),
            (value, factor, delta)
                => ConvertRoundtrip(Compose(Scale(factor), Offset(delta)), value) == value)
        .QuickCheckThrowOnFailure();
    }

    [Test]
    public void Affine_should_be_syntaxic_suger_of_composition()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            Arbitraries.Rational().Filter(v => v != Rational.Zero),
            Arbitraries.Rational(),
            (value, factor, delta) =>
            {
                var affine = Affine(factor, delta);
                var compose = Compose(Scale(factor), Offset(delta));
                return ConvertRoundtrip(affine, value) == ConvertRoundtrip(compose, value);
            })
        .QuickCheckThrowOnFailure();
    }

    private static Rational ConvertRoundtrip(Converter converter, Rational value)
    {
        var first = converter.From(converter.To(value));
        return converter.To(converter.From(first));
    }
}
