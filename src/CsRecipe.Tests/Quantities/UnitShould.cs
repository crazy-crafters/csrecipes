using CsRecipe.Quantities;
using Rationals;
using static CsRecipe.Quantities.Converter;
using static CsRecipe.Quantities.Unit;

namespace CsRecipe.Tests.Quantities;

public class UnitShould
{
    [Test]
    public void Define_base_unit_name()
    {
        var unit = Base("gram", "g");
        Assert.That(unit.Name, Is.EqualTo("gram"));
    }

    [Test]
    public void Define_base_unit_symbol()
    {
        var unit = Base("gram", "g");
        Assert.That(unit.Symbol, Is.EqualTo("g"));
    }

    [Test]
    public void Define_base_unit_formula()
    {
        var unit = Base("gram", "g");
        Assert.That(unit.To(1), Is.EqualTo(1));
    }

    [Test]
    public void Define_base_unit_root()
    {
        var unit = Base("gram", "g");
        Assert.That(unit.Root, Is.EqualTo(unit));
    }

    [Test]
    public void Define_composite_unit_name()
    {
        var unit = Compose(
            Prefix.Kilo,
            Base("gram", "g"));
        Assert.That(unit.Name, Is.EqualTo("kilogram"));
    }

    [Test]
    public void Define_composite_unit_symbol()
    {
        var unit = Compose(
            Prefix.Kilo,
            Base("gram", "g"));
        Assert.That(unit.Symbol, Is.EqualTo("kg"));
    }

    [Test]
    public void Define_composite_unit_formula()
    {
        var unit = Compose(
            Prefix.Kilo,
            Base("gram", "g"));
        Assert.That(unit.To(1), Is.EqualTo(1000));
    }

    [Test]
    public void Define_composite_unit_root()
    {
        var g = Base("gram", "g");
        var kg = Compose(Prefix.Kilo, g);
        Assert.That(kg.Root, Is.EqualTo(g));
    }

    [Test]
    public void Define_derived_unit_name()
    {
        var unit = Derived(
            "calorie",
            "cal",
            Base("joule", "J"),
            Scale(new Rational(4184, 1000)));
        Assert.That(unit.Name, Is.EqualTo("calorie"));
    }

    [Test]
    public void Define_derived_unit_symbol()
    {
        var unit = Derived(
            "calorie",
            "cal",
            Base("joule", "J"),
            Scale(new Rational(4184, 1000)));
        Assert.That(unit.Symbol, Is.EqualTo("cal"));
    }

    [Test]
    public void Define_derived_unit_formula()
    {
        var unit = Derived(
            "calorie",
            "cal",
            Base("joule", "J"),
            Scale(new Rational(4184, 1000)));
        Assert.That(unit.To(1000), Is.EqualTo(4184));
    }

    [Test]
    public void Define_derived_unit_root()
    {
        var j = Base("joule", "J");
        var cal = Derived(
            "calorie",
            "cal",
            j,
            Scale(new Rational(4184, 1000)));
        Assert.That(cal.Root, Is.EqualTo(j));
    }
}
