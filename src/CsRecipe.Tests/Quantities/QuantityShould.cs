using CsRecipe.Quantities;
using FsCheck;
using FsCheck.Fluent;
using static CsRecipe.Quantities.Units;

namespace CsRecipe.Tests.Quantities;

public class QuantityShould
{
    [Test]
    public void Render_into_human_form()
    {
        Assert.That(Quantity.Of(2, kg).Render(), Is.EqualTo("2 kg"));
    }

    [Test]
    public void Convert_into_unit_with_same_base_unit()
    {
        Assert.That(
            Quantity.Of(2, kg).Convert(g).Render(),
            Is.EqualTo("2000 g"));
    }

    [Test]
    public void Forbid_convertion_of_different_base_units()
    {
        Assert.That(
            () => Quantity.Of(2, kg).Convert(l),
            Throws.InstanceOf<ArgumentException>());
    }

    [Test]
    public void Convert_roundtrip_shoulb_be_identity()
    {
        Prop.ForAll(
            Arbitraries.Rational(),
            (value) =>
            {
                var quantity = Quantity.Of(value, g);
                return quantity == quantity.Convert(kg).Convert(g);
            })
        .QuickCheckThrowOnFailure();
    }
}
