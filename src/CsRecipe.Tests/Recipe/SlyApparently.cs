using sly.lexer;
using sly.parser;
using sly.parser.generator;

namespace CsRecipe.Tests.Recipe;

public class SlyApparently
{
    public enum ExpressionToken
    {
        [Lexeme("[0-9]+")]
        INT = 1,

        [Lexeme("\\+")]
        PLUS = 2,

        [Lexeme("[ \\t]+", isSkippable: true)]
        WS = 3,
    }

    public class ExpressionParser
    {
        [Production("expression: INT")]
        public int intExpr(Token<ExpressionToken> intToken)
        {
            return intToken.IntValue;
        }

        [Production("expression: term PLUS expression")]
        public int Expression(int left, Token<ExpressionToken> operatorToken, int right)
        {
            return left + right;
        }

        [Production("term: INT")]
        public int Expression(Token<ExpressionToken> intToken)
        {
            return intToken.IntValue;
        }

        public static Parser<ExpressionToken, int> GetParser()
        {
            var parserInstance = new ExpressionParser();
            var builder = new ParserBuilder<ExpressionToken, int>();
            var parser = builder.BuildParser(
                parserInstance, ParserType.LL_RECURSIVE_DESCENT, "expression").Result;
            return parser;
        }

        public static ParseResult<ExpressionToken, int> Parse(string expression)
        {
            var Parser = ExpressionParser.GetParser();
            return Parser.Parse(expression);
        }
    }

    [Test]
    public void Bla_1()
    {
        var result = ExpressionParser.Parse("2 + 3");
        Assert.That(result.Result, Is.EqualTo(5));
    }
}
