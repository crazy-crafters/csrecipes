using ParsecSharp;
using static ParsecSharp.Parser;
using static ParsecSharp.Text;

namespace CsRecipe.Tests.Recipe;

public class ParsecSharpApparently
{
    [Test]
    public void Bla_1()
    {
        var result = String("aa").Parse("aaa");
        var value = result.CaseOf(
            failure => false,
            success => true);
        Assert.That(value, Is.True);
    }

    [Test]
    public void Bla_2()
    {
        var result = String("aa").Parse("bb");
        var value = result.CaseOf(
            failure => false,
            success => true);
        Assert.That(value, Is.False);
    }

    [Test]
    public void Bla_3()
    {
        var colon = Char(':').Between(Spaces()).AsString();
        var recipe_token = Sequence(String("recipe"), colon);
        var str = Many1(Letter()).AsString();

        var recipe =
            from _ in recipe_token
            from name in str
            select new { name };

        var result = recipe.Parse("recipe: Couscous").Value;
        Assert.That(result.name, Is.EqualTo("Couscous"));
    }
}
