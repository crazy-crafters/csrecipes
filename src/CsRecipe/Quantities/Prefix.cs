using Rationals;

namespace CsRecipe.Quantities;

public record Prefix(string Name, string Symbol, Converter Converter)
{
    public static readonly Prefix Micro = new Prefix(
        "micro",
        "µ",
        Converter.Scale(new Rational(1, 1000000)));

    public static readonly Prefix Milli = new Prefix(
        "milli",
        "m",
        Converter.Scale(new Rational(1, 1000)));

    public static readonly Prefix Centi = new Prefix(
        "centi",
        "c",
        Converter.Scale(new Rational(1, 100)));

    public static readonly Prefix Deci = new Prefix(
        "deci",
        "d",
        Converter.Scale(new Rational(1, 10)));

    public static readonly Prefix Kilo = new Prefix(
        "kilo",
        "k",
        Converter.Scale(new Rational(1000)));
}
