using Rationals;

namespace CsRecipe.Quantities;

public record Unit
{
    private sealed record BaseUnit(string Name_, string Symbol_)
        : Unit;
    private sealed record CompositeUnit(Prefix Prefix_, Unit Unit_)
        : Unit;
    private sealed record DerivedUnit(string Name_, string Symbol_, Unit Unit_, Converter Formula_)
        : Unit;

    public static Unit Base(string name, string symbol) => new BaseUnit(name, symbol);

    public static Unit Derived(string name, string symbol, Unit unit, Converter formula) => new DerivedUnit(name, symbol, unit, formula);

    public static Unit Compose(Prefix prefix, Unit unit) => new CompositeUnit(prefix, unit);

    public string Name => this switch
    {
        BaseUnit(var name, _) => name,
        CompositeUnit(var prefix, var unit) => prefix.Name + unit.Name,
        DerivedUnit(var name, _, _, _) => name,
        _ => throw new ArgumentException(),
    };

    public string Symbol => this switch
    {
        BaseUnit(_, var symbol) => symbol,
        CompositeUnit(var prefix, var unit) => prefix.Symbol + unit.Symbol,
        DerivedUnit(_, var symbol, _, _) => symbol,
        _ => throw new ArgumentException(),
    };

    public Converter Formula => this switch
    {
        BaseUnit => Converter.Identity(),
        CompositeUnit(var prefix, var unit) => Converter.Compose(
                prefix.Converter,
                unit.Formula),
        DerivedUnit(_, _, var unit, var formula) => Converter.Compose(formula, unit.Formula),
        _ => throw new ArgumentException(),
    };

    public Rational To(Rational value) => Formula.To(value).CanonicalForm;

    public Rational From(Rational value) => Formula.From(value).CanonicalForm;

    public Unit Root => this switch
    {
        BaseUnit(_, _) => this,
        CompositeUnit(_, var unit) => unit.Root,
        DerivedUnit(_, _, var unit, _) => unit.Root,
        _ => throw new ArgumentException(),
    };

    private Unit()
    {
    }
}
