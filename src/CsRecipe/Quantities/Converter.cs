using System.Security.Cryptography;
using System.Text.RegularExpressions;
using Rationals;

namespace CsRecipe.Quantities;

public record Converter
{
    public sealed record IdentityConverter : Converter;
    public sealed record ScaleConverter(Rational Factror) : Converter;
    public sealed record OffsetConverter(Rational Delta) : Converter;
    public sealed record InverseConverter(Converter Inner) : Converter;
    public sealed record CompositeConverter(Converter First, Converter Second) : Converter;

    public static Converter Identity() => new IdentityConverter();

    public static Converter Scale(Rational factor)
    {
        ArgumentOutOfRangeException.ThrowIfEqual(factor, Rational.Zero);
        return new ScaleConverter(factor);
    }

    public static Converter Offset(Rational delta) => new OffsetConverter(delta);

    public static Converter Affine(Rational a, Rational b) => Compose(Scale(a), Offset(b));

    public static Converter Inverse(Converter inner) => new InverseConverter(inner);

    public static Converter Compose(Converter first, Converter second) => new CompositeConverter(first, second);

    public Rational To(Rational value) => this switch
    {
        ScaleConverter(var factor) => value * factor,
        OffsetConverter(var delta) => value + delta,
        InverseConverter(var inner) => inner.From(value),
        CompositeConverter(var first, var second) => second.To(first.To(value)),
        _ => value,
    };

    public Rational From(Rational value) => this switch
    {
        ScaleConverter(var factor) => value / factor,
        OffsetConverter(var delta) => value - delta,
        InverseConverter(var inner) => inner.To(value),
        CompositeConverter(var first, var second) => first.From(second.From(value)),
        _ => value,
    };

    private Converter()
    {
    }
}
