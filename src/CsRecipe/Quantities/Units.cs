using Rationals;

using static CsRecipe.Quantities.Converter;
using static CsRecipe.Quantities.Prefix;
using static CsRecipe.Quantities.Unit;

namespace CsRecipe.Quantities;

public class Units
{
#pragma warning disable SA1307 // Accessible fields should begin with upper-case letter
#pragma warning disable SA1311 // Static readonly fields should begin with upper-case letter

    // Mass
    public static readonly Unit g = Base("gram", "g");
    public static readonly Unit ug = Compose(Micro, g);
    public static readonly Unit mg = Compose(Milli, g);
    public static readonly Unit kg = Compose(Kilo, g);

    // Volume
    public static readonly Unit l = Base("litre", "l");
    public static readonly Unit ml = Compose(Milli, l);
    public static readonly Unit cl = Compose(Centi, l);
    public static readonly Unit dl = Compose(Deci, l);
    public static readonly Unit cup = Derived("cup", "cup", ml, Scale(250));
    public static readonly Unit handful = Derived("handful", "handful", ml, Scale(125));
    public static readonly Unit tbs = Derived("table spoon", "tbs", ml, Scale(15));
    public static readonly Unit tsp = Derived("tea spoon", "tsp", ml, Scale(5));
    public static readonly Unit pinch = Derived("pinch", "pinch", ml, Scale(new Rational(1, 2)));

#pragma warning restore SA1311 // Static readonly fields should begin with upper-case letter
#pragma warning restore SA1307 // Accessible fields should begin with upper-case letter
}
