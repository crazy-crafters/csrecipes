using Rationals;

namespace CsRecipe.Quantities;

public record Quantity
{
    private readonly Rational magnitude;
    private readonly Unit unit;

    private Quantity(Rational magnitude, Unit unit)
    {
        this.magnitude = magnitude;
        this.unit = unit;
    }

    public Rational Magnitude
    {
        get => unit.From(magnitude);
    }

    public Unit Unit
    {
        get => unit;
    }

    public static Quantity Of(Rational magnitude, Unit unit)
    {
        return new Quantity(unit.To(magnitude), unit);
    }

    public Quantity Convert(Unit new_unit)
    {
        if (unit.Root != new_unit.Root)
        {
            throw new ArgumentException();
        }

        return new Quantity(magnitude, new_unit);
    }

    public string Render()
    {
        return $"{Magnitude} {unit.Symbol}";
    }
}
