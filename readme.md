# csrecipe

csrecipe is a tool suite for managing cooking recipes.

🍽️

## Development

Run the tests: `dotnet test src`


## What we want to do?

Thématiques principales

- quantités / unités / formats pivots (gestion des quantité)
- modélisation d'une recette / parsing / grammaires ...

## Brain dumps

Big picture

```mermaid
mindmap
  root((csrecipe))
    C#
        Language
            Class with constant?
        Dependencies
            Rationale
    Vim

    Jetbrains
        Test until failure
    Modeling cooking recipes

    Handling quantities
```

Handling quantities

```mermaid
mindmap
  root((Handling quantities))
    Rational numbers
    Converter
        API
            Rational -> Rational
            from
            to
        Identity
        Affine
        Composite
        Inverse
    Units = instances
        ex_units["`ex.
            k = Prefix('kilo','k',(), x1000)
            g = Base('gram','g')
            kg = Composite(k,g)
            lbs =Derived('pounds','lbs',g x 454.59237)`"]

    Unit definition
        Base Unit
            ex.
                g
                kilo
                k
        Composite
            ex. kg
        Derived
            ex. lbs : Joule vs Calories = conversion
```

```sh
dotnet add CsRecipes.Test reference CsRecipe
dotnet add CsRecipe package Rationals
dotnet remove CsRecipe.Test package Rationals
```

## Recipe

```

recipe: Grape Couscous @dehydrated.Couscous
    source: myself
    lang: en
    servings: 1
    description:
        A smart description

    --
    <80 g> of couscous @couscous
    <20 g> dried grapes @fruit.grapes|dried
    <3 g> vegetable broth powder @vegetable-broth
    <3 g> dehydrated leeks
    <7> chickpeas
    <some> salt
    Mix all ingredients together.
    --
    <40 cl> water
    Add water at a minimum temperature of [90 °C] and wait [8--10 min].
```

### PEG : Parsing Expression Grammar


- https://dzone.com/articles/parsing-in-java-part-3-diving-into-peg-parsers

- [ParserSharp tutorial](https://github.com/acple/ParsecSharp/blob/dev/UnitTest.ParsecSharp/Tutorial.cs)

```mermaid
mindmap
  root((Model cooking recipes))
    Parser
        PEG
            defined["Defined in its target language"] 
            no_preprocessing["Does not require preprocessing"]
        
    C#
        Dependencies
            Pegasus
                PEG Grammar is not expressed as plain C# code
            SharpPeg
            pointloader/peg
            npeg
                last update 5 years ago
            ParsecSharp["ParsecSharp : https://github.com/acple/ParsecSharp"]
                
    Vim

    Jetbrains
        Test until failure
    Modeling cooking recipes

    Handling quantities
```

## Fridge

Interesting questions and Ideas we don't want to address in the scope of this repo.

- Division par zéro possible dans la création d'un affine converter (ex. dans la factory ou dans le construction)
- https://docs.nunit.org/articles/nunit/release-notes/Nunit4.0-MigrationGuide.html
- https://github.com/Olical/vim-enmasse
- How to make FsCheck Rational Generator global?
- Syntax Highlighting?
- LSP?
  - Parsing?
- In browser code editor
- Parse error messages?
- Completion?
- In browser syntax highlighted recipe editor